import React from 'react';
import { MdEventNote } from 'react-icons/md';
import '../style/header.less'
import {NavLink} from "react-router-dom";

export default () => {
    return (
        <header className="header">
            <NavLink to='/'>商城</NavLink>
            <NavLink to='/orders'>订单</NavLink>
            <NavLink to='/add-production'>添加商品</NavLink>
        </header>
    )
};