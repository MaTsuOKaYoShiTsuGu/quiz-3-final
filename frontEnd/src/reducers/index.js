import {combineReducers} from "redux";
import productionReducer from "../onlineStore/reducers/productionReducer";
const reducers = combineReducers({
    production : productionReducer
});
export default reducers;