const initState = {
    productionDetails: {}
};

export default (state = initState, action) => {
    if (action.type === 'GET_PRODUCTION_DETAILS') {
        return {
            ...state,
            productionDetails: action.productionDetails
        };
    } else {
        return state
    }
};
