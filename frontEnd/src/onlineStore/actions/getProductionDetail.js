export const getProductionDetails = () => (dispatch) => {
    fetch("http://localhost:8080/api/production")
        .then(response => response.json())
        .then(result=>{
            dispatch({
                type: 'GET_PRODUCTION_DETAILS',
                productionDetails: result
            });
        });
};