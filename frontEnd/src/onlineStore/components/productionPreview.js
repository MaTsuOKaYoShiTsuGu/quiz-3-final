import React, {Component} from 'react';

class ProductionPreview extends Component {

    constructor(props, context) {
        super(props, context);
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit(){

    }

    render() {
        return (
            <div>
                <img src={this.props.details.pictureURL} alt='picture not exist'/>
                <p>{this.props.details.name}</p>
                <p>{this.props.details.price}</p>
                <button onClick={this.onSubmit}>添加按钮</button>
            </div>
        );
    }
}

export default ProductionPreview;