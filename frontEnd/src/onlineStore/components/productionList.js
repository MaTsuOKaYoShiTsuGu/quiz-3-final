import React, {Component} from 'react';
import ProductionPreview from "./productionPreview";
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import {getProductionDetails} from '../actions/getProductionDetail'
class ProductionList extends Component {

    constructor(props, context) {
        super(props, context);
        this.renderProductionList = this.renderProductionList.bind(this)
    }

    componentDidMount() {
        this.props.getProductionDetails();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.props.getProductionDetails();
    }

    renderProductionList(){
        console.log(this.props.productionDetails);
        return Object.keys(this.props.productionDetails).map(index => {
            let {id} = this.props.productionDetails[index];
            return <li key={id}><ProductionPreview details={this.props.productionDetails[index]}/></li>;
        });
    }

    render() {
        return (
            <div>
                {this.renderProductionList()}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    productionDetails: state.production.productionDetails
});

const mapDispatchToProps = dispatch => bindActionCreators({
    getProductionDetails
}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ProductionList);