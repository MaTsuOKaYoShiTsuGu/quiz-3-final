import React, {Component} from 'react';
import ProductionList from "../components/productionList";

class HomePage extends React.Component {
    render() {
        return (
            <div className='product-list'>
                <div>主页</div>
                <ProductionList/>
            </div>
        );
    }
}

export default HomePage;