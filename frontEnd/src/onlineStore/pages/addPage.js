import React from 'react';
class AddPage extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.state = {
            name:"",
            price:0,
            unit:"",
            pictureURL:""
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.nameOnChange = this.nameOnChange.bind(this);
        this.priceOnChange = this.priceOnChange.bind(this);
        this.unitOnChange = this.unitOnChange.bind(this);
        this.pictureURLOnChange = this.pictureURLOnChange.bind(this);
        this.verifyParam = this.verifyParam.bind(this);
    }

    nameOnChange(event){
        this.setState({
            name:event.target.value
        });
    }

    priceOnChange(event){
        this.setState({
            price:event.target.value
        });
    }

    unitOnChange(event){
        this.setState({
            unit:event.target.value
        });
    }

    pictureURLOnChange(event){
        this.setState({
            pictureURL:event.target.value
        });
    }

    verifyParam(){
        return this.state.name !==""&&this.state.price !==""&&this.state.unit!==""&&this.state.pictureURL!=="";
    }

    onSubmit() {
        fetch("http://localhost:8080/api/add-production", {
            method: 'POST',
            headers: {
                'content-type': 'application/json;charset=UTF-8',
                'Access-Control-Allow-Origin': 'http://127.0.0.1:1234'
            },
            body: JSON.stringify({
                name: this.state.name,
                price: this.state.price,
                unit: this.state.unit,
                pictureURL: this.state.pictureURL
            })
        }).catch(err => {
            console.log(err);
        });
        this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <h2>添加商品</h2>
                <p>*名称：</p>
                <input onChange={this.nameOnChange}/>
                <p>*价格</p>
                <input onChange={this.priceOnChange}/>
                <p>*单位</p>
                <input onChange={this.unitOnChange}/>
                <p>*图片</p>
                <input onChange={this.pictureURLOnChange}/>
                <br/>
                <button type='submit' onClick={this.onSubmit} disabled={!this.verifyParam()}>提交</button>
            </div>
        );
    }
}

export default AddPage;