import React, {Component} from 'react';
import {BrowserRouter as Router, NavLink} from "react-router-dom";
import Header from './public/header'
import './App.less';
import {Switch} from 'react-router-dom'
import {Route} from "react-router-dom";
import HomePage from "./onlineStore/pages/homePage";
import OrderPage from "./onlineStore/pages/orderPage";
import AddPage from "./onlineStore/pages/addPage";

class App extends Component {
    render() {
        return (
            <Router>
                <div className='App'>
                    <Header/>
                </div>
                <Switch>
                    <Route exact path='/' component={HomePage}/>
                    <Route exact path='/orders' component={OrderPage}/>
                    <Route exact path='/add-production' component={AddPage}/>
                </Switch>
            </Router>
        );
    }
}

export default App;