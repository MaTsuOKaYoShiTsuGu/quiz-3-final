package com.twuc.webApp.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.contracts.CreateProductionRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class ProductionControllerTest {
    @Autowired
    private MockMvc mockMvc;

    private ObjectMapper objectMapper = new ObjectMapper();

    private void addProduction(String name,Double price,String unit,String pictureURL) throws Exception {
        CreateProductionRequest production = new CreateProductionRequest(
                name,
                price,
                unit,
                pictureURL
        );
        mockMvc.perform(post("/api/add-production")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsString(production)))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }
    @Test
    void should_save_production_into_database_and_return_201() throws Exception {
        addProduction("cola",1.0,"瓶","none");
    }

    @Test
    void should_get_production_after_saving_and_return_200() throws Exception {
        addProduction("cola",1.0,"瓶","none");
        mockMvc.perform(get("/api/production"))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(jsonPath("$[0].name").value("cola"));
    }

    @Test
    void should_delete_production_and_return_200_when_production_exist() throws Exception {
        addProduction("cola",1.0,"瓶","none");
        mockMvc.perform(delete("/api/production/1"))
                .andExpect(MockMvcResultMatchers.status().is(200));
    }

}
