package com.twuc.webApp.contracts;

import javax.persistence.Column;

public class CreateProductionRequest {
    @Column
    private String name;

    @Column
    private Double price;

    @Column
    private String unit;

    @Column
    private String pictureURL;

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureURL() {
        return pictureURL;
    }

    public CreateProductionRequest(String name, Double price, String unit, String pictureURL) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureURL = pictureURL;
    }

    public CreateProductionRequest() {
    }
}
