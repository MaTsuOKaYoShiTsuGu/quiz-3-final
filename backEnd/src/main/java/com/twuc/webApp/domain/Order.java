package com.twuc.webApp.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Long getId() {
        return id;
    }

    @OneToMany(mappedBy = "order")
    private List<ProductionDetails>productions = new ArrayList<>();

    public List<ProductionDetails> getProductions() {
        return productions;
    }

    public Order() {
    }
}
