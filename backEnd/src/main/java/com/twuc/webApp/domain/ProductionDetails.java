package com.twuc.webApp.domain;

import javax.persistence.*;

@Entity
public class ProductionDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private Double price;

    @Column
    private String unit;

    @Column
    private String pictureURL;

    @ManyToOne
    private Order order;

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public ProductionDetails() {
    }

    public ProductionDetails(String name, Double price, String unit, String pictureURL) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.pictureURL = pictureURL;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getUnit() {
        return unit;
    }

    public String getPictureURL() {
        return pictureURL;
    }
}
