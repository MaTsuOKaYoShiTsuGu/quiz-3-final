package com.twuc.webApp.web;

import com.twuc.webApp.contracts.CreateProductionRequest;
import com.twuc.webApp.domain.ProductionDetails;
import com.twuc.webApp.domain.ProductionRepository;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ProductionController {
    private final ProductionRepository productionRepository;

    public ProductionController(ProductionRepository productionRepository) {
        this.productionRepository = productionRepository;
    }

    @PostMapping("/api/add-production")
    ResponseEntity createProduction(@RequestBody CreateProductionRequest productionRequest){
        ProductionDetails production = new ProductionDetails(
                productionRequest.getName(),
                productionRequest.getPrice(),
                productionRequest.getUnit(),
                productionRequest.getPictureURL()
        );

        productionRepository.save(production);

        return ResponseEntity.status(201).body("");
    }

    @GetMapping("/api/production")
    ResponseEntity getProductions(){
        List<ProductionDetails> productions = productionRepository.findAll();
        return ResponseEntity.status(200).body(productions);
    }

    @DeleteMapping("/api/production/{productionId}")
    ResponseEntity deleteProduction(@PathVariable Long productionId){
        if(productionRepository.findById(productionId).isPresent()){
            productionRepository.deleteById(productionId);
            return ResponseEntity.status(200).build();
        }
        else
            return ResponseEntity.status(400).build();
    }
}
